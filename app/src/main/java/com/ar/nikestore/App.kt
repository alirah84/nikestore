package com.ar.nikestore

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import com.ar.nikestore.data.repo.*
import com.ar.nikestore.data.repo.source.*
import com.ar.nikestore.feature.auth.AuthViewModel
import com.ar.nikestore.feature.cart.CartViewModel
import com.ar.nikestore.feature.list.ProductListViewModel
import com.ar.nikestore.feature.home.HomeViewModel
import com.ar.nikestore.feature.common.ProductListAdapter
import com.ar.nikestore.feature.product.ProductDetailViewModel
import com.ar.nikestore.feature.product.comment.CommentListViewModel
import com.ar.nikestore.services.FrescoImageLoadingService
import com.ar.nikestore.services.ImageLoadingService
import com.ar.nikestore.services.http.createApiServiceInstance
import com.facebook.drawee.backends.pipeline.Fresco
import org.koin.android.ext.android.get
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
import timber.log.Timber

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        Fresco.initialize(this)

        val myModules = module {
            single<SharedPreferences> {getSharedPreferences("app_setting", Context.MODE_PRIVATE)}
            single { createApiServiceInstance() }
            single <ImageLoadingService> {FrescoImageLoadingService()}
            factory<ProductRepository> { ProductRepositoryImpl(ProductRemoteDataSource(get()),
                ProductLocalDataSource()
            )}
            factory <BannerRepository> {BannerRepositoryImpl(BannerRemoteDataSource(get()))}
            factory<CommentRepository> { CommentRepositoryImpl(CommentRemoteDataSource(get())) }
            factory<CartRepository> {CartRepositoryImpl(CartRemoteDataSource(get()))}
            factory {(viewType:Int)->
                ProductListAdapter(
                    viewType,
                    get()
                )
            }
            single<UserRepository> {UserRepositoryImpl(UserLocalDataSource(get()),UserRemoteDataSource(get()))}
            single<UserDataSource> {UserLocalDataSource(get())}
            viewModel {
                HomeViewModel(
                    get(),
                    get()
                )
            }
            viewModel { (bundle: Bundle)-> ProductDetailViewModel(bundle,get(),get()) }
            viewModel { (productId : Int)-> CommentListViewModel(productId,get()) }
            viewModel { (sort:Int) -> ProductListViewModel(sort,get()) }
            viewModel { AuthViewModel(get()) }
            viewModel { CartViewModel(get()) }
        }

        startKoin {
            androidContext(this@App)
            modules(myModules)
        }
        val userRepository:UserRepository = get()
        userRepository.loadToken()
    }
}