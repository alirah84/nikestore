package com.ar.nikestore.services

import com.ar.nikestore.view.NikeImageView

interface ImageLoadingService {
    fun load(imageView:NikeImageView,imageUrl:String)
}