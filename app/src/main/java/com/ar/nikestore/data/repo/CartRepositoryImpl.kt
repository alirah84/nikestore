package com.ar.nikestore.data.repo

import com.ar.nikestore.data.AddToCartResponse
import com.ar.nikestore.data.CartItemCount
import com.ar.nikestore.data.CartResponse
import com.ar.nikestore.data.MessageResponse
import com.ar.nikestore.data.repo.source.CartDataSource
import com.ar.nikestore.data.repo.source.CartRemoteDataSource
import io.reactivex.Single

class CartRepositoryImpl(val cartRemoteDataSource: CartDataSource):CartRepository {
    override fun addToCart(productId: Int): Single<AddToCartResponse> = cartRemoteDataSource.addToCart(productId)

    override fun get(): Single<CartResponse>  = cartRemoteDataSource.get()

    override fun remove(cartItemId: Int): Single<MessageResponse>  = cartRemoteDataSource.remove(cartItemId)

    override fun changeCount(cartItemId: Int, count: Int): Single<AddToCartResponse> = cartRemoteDataSource.changeCount(cartItemId,count)

    override fun getCartItemsCount(): Single<CartItemCount> = cartRemoteDataSource.getCartItemsCount()
}