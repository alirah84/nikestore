package com.ar.nikestore.data.repo.source

import com.ar.nikestore.data.Banner
import com.ar.nikestore.services.http.ApiService
import io.reactivex.Single

class BannerRemoteDataSource(val apiService: ApiService) : BannerDataSource {
    override fun getBanners(): Single<List<Banner>> = apiService.getBanners()
}