package com.ar.nikestore.data

data class MessageResponse(
        val message: String
)