package com.ar.nikestore.data.repo

import com.ar.nikestore.data.Product
import com.ar.nikestore.data.repo.source.ProductDataSource
import com.ar.nikestore.data.repo.source.ProductRemoteDataSource
import io.reactivex.Completable
import io.reactivex.Single

class ProductRepositoryImpl(val remoteDataSource: ProductDataSource,val productLocalDataSource: ProductDataSource):ProductRepository {
    override fun getProducts(sort:Int): Single<List<Product>> = remoteDataSource.getProducts(sort)

    override fun getFavProducts(): Single<List<Product>> {
        TODO("Not yet implemented")
    }

    override fun addToFavorites(): Completable {
        TODO("Not yet implemented")
    }

    override fun deleteFromFavorites(): Completable {
        TODO("Not yet implemented")
    }
}