package com.ar.nikestore.data

data class Author(
    val email: String
)