package com.ar.nikestore.data.repo.source

import com.ar.nikestore.data.Banner
import io.reactivex.Single

interface BannerDataSource {
    fun getBanners(): Single<List<Banner>>
}