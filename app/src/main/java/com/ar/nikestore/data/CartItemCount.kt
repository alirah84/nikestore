package com.ar.nikestore.data

data class CartItemCount(
        val count: Int
)