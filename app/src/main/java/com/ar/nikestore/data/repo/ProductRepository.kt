package com.ar.nikestore.data.repo

import com.ar.nikestore.data.Product
import io.reactivex.Completable
import io.reactivex.Single

interface ProductRepository {
    fun getProducts(sort:Int):Single<List<Product>>

    fun getFavProducts():Single<List<Product>>

    fun addToFavorites():Completable

    fun deleteFromFavorites():Completable
}