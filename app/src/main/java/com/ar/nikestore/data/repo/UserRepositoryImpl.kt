package com.ar.nikestore.data.repo

import com.ar.nikestore.data.TokenContainer
import com.ar.nikestore.data.TokenResponse
import com.ar.nikestore.data.repo.source.UserDataSource
import com.ar.nikestore.data.repo.source.UserLocalDataSource
import com.ar.nikestore.data.repo.source.UserRemoteDataSource
import io.reactivex.Completable

class UserRepositoryImpl(val userLocalDataSource: UserDataSource,val userRemoteDataSource: UserDataSource):UserRepository {
    override fun login(userName: String, password: String): Completable {
        return userRemoteDataSource.login(userName,password).doOnSuccess {
            onSuccessfulLogin(it)
        }.ignoreElement()
    }

    override fun signUp(userName: String, password: String): Completable {
        return userRemoteDataSource.signUp(userName,password).flatMap {
            userRemoteDataSource.login(userName, password)
        }.doOnSuccess {
            onSuccessfulLogin(it)
        }.ignoreElement()
    }

    override fun loadToken() {
        userLocalDataSource.loadToken()
    }

    fun onSuccessfulLogin(tokenResponse: TokenResponse){
        TokenContainer.update(tokenResponse.access_token,tokenResponse.refresh_token)
        userLocalDataSource.saveToken(tokenResponse.access_token,tokenResponse.refresh_token)
    }
}