package com.ar.nikestore.data.repo.source

import com.ar.nikestore.data.AddToCartResponse
import com.ar.nikestore.data.CartItemCount
import com.ar.nikestore.data.CartResponse
import com.ar.nikestore.data.MessageResponse
import io.reactivex.Single

interface CartDataSource {
    fun addToCart(productId:Int): Single<AddToCartResponse>
    fun get():Single<CartResponse>
    fun remove(cartItemId: Int):Single<MessageResponse>
    fun changeCount(cartItemId:Int,count:Int):Single<AddToCartResponse>
    fun getCartItemsCount():Single<CartItemCount>
}