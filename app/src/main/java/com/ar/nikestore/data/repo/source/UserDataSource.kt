package com.ar.nikestore.data.repo.source

import com.ar.nikestore.data.MessageResponse
import com.ar.nikestore.data.TokenResponse
import io.reactivex.Completable
import io.reactivex.Single

interface UserDataSource {
    fun login(userName:String,password:String):Single<TokenResponse>
    fun signUp(userName:String,password:String):Single<MessageResponse>
    fun loadToken()
    fun saveToken(token:String,refreshToken:String)
}