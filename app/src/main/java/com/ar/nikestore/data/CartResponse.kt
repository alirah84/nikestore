package com.ar.nikestore.data

data class CartResponse(
        val cart_items: List<CartItem>,
        val payable_price: Int,
        val shipping_cost: Int,
        val total_price: Int
)

data class PurchaseDetail(
        var totalPrice:Int,
        var payablePrice: Int,
        var shippingCost: Int
)