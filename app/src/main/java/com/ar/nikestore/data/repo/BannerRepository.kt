package com.ar.nikestore.data.repo

import com.ar.nikestore.data.Banner
import io.reactivex.Single

interface BannerRepository {
    fun getBanners(): Single<List<Banner>>
}