package com.ar.nikestore.feature.product

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import com.ar.nikestore.common.EXTRA_KEY_DATA
import com.ar.nikestore.common.NikeSingleObserver
import com.ar.nikestore.common.NikeViewModel
import com.ar.nikestore.common.asyncNetworkRequest
import com.ar.nikestore.data.Comment
import com.ar.nikestore.data.Product
import com.ar.nikestore.data.repo.CartRepository
import com.ar.nikestore.data.repo.CommentRepository
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ProductDetailViewModel(bundle: Bundle,commentRepository: CommentRepository,val cartRepository: CartRepository): NikeViewModel() {
   val productMutableLiveData = MutableLiveData<Product>()
    val commentMutableLiveData = MutableLiveData<List<Comment>>()
    init {
        productMutableLiveData.value = bundle.getParcelable(EXTRA_KEY_DATA)
        progressBarLiveData.value = true
        commentRepository.getAll(productMutableLiveData.value!!.id)
            .asyncNetworkRequest()
            .doFinally{
                progressBarLiveData.value = false
            }
            .subscribe(object :NikeSingleObserver<List<Comment>>(compositeDisposable){
                override fun onSuccess(t: List<Comment>) {
                    commentMutableLiveData.value = t
                }

            })
    }

    fun onAddToCartBtn():Completable = cartRepository.addToCart(productMutableLiveData.value!!.id).ignoreElement()
}
