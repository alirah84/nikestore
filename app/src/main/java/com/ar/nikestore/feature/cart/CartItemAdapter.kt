package com.ar.nikestore.feature.cart

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ar.nikestore.R
import com.ar.nikestore.common.formatPrice
import com.ar.nikestore.data.CartItem
import com.ar.nikestore.data.PurchaseDetail
import com.ar.nikestore.services.ImageLoadingService
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_cart.view.*
import kotlinx.android.synthetic.main.item_purchase_detail.view.*
const val VIEW_TYPE_CART_ITEM = 0
const val VIEW_TYPE_PURCHASE_DETAIL = 1
class CartItemAdapter(val cartItems:MutableList<CartItem>,val cartItemViewCallBacks: CartItemViewCallBacks
                      ,val imageLoadingService: ImageLoadingService)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var purchaseDetail:PurchaseDetail? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType== VIEW_TYPE_CART_ITEM){
            return CartItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_cart,parent,false))
        }
        else
            return PurchaseDetailViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_purchase_detail,parent,false))
    }

    override fun getItemCount(): Int = cartItems.size + 1

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CartItemViewHolder){
            holder.bindCartItem(cartItems[position])
        }
        else if (holder is PurchaseDetailViewHolder){
            purchaseDetail?.let {
                holder.bind(it.totalPrice,it.shippingCost,it.payablePrice)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (position==cartItems.size){
            return VIEW_TYPE_PURCHASE_DETAIL
        }
        else
            return VIEW_TYPE_CART_ITEM
    }
    inner class CartItemViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),LayoutContainer{
        fun bindCartItem(cartItem: CartItem){
            containerView.productTitleIv.text = cartItem.product.title
            containerView.cartItemCountTv.text = cartItem.count.toString()
            containerView.previousPriceTv.text = formatPrice(cartItem.product.price+cartItem.product.discount)
            containerView.currentPriceTv.text = formatPrice(cartItem.product.price)
            imageLoadingService.load(containerView.productIv,cartItem.product.image)
            containerView.removeFromCartBtn.setOnClickListener {
                cartItemViewCallBacks.onRemoveCartItemButtonClick(cartItem)
            }
            containerView.increaseBtn.setOnClickListener {
                cartItem.changeCountProgressBarIsVisible = true
                containerView.changeCountProgressBar.visibility = View.VISIBLE
                containerView.cartItemCountTv.visibility = View.INVISIBLE
                cartItemViewCallBacks.onIncreaseCartItemButtonClick(cartItem)
            }
            containerView.decreaseBtn.setOnClickListener {
                if (cartItem.count>1){
                    cartItem.changeCountProgressBarIsVisible = true
                    containerView.changeCountProgressBar.visibility = View.VISIBLE
                    containerView.cartItemCountTv.visibility = View.INVISIBLE
                    cartItemViewCallBacks.onDecreaseCartItemButtonClick(cartItem)
                }
            }

            containerView.productIv.setOnClickListener {
                cartItemViewCallBacks.onProductImageClick(cartItem)
            }
            containerView.changeCountProgressBar.visibility = if (cartItem.changeCountProgressBarIsVisible) View.VISIBLE else View.GONE
            containerView.cartItemCountTv.visibility = if (cartItem.changeCountProgressBarIsVisible) View.INVISIBLE else View.VISIBLE
        }
    }

    inner class PurchaseDetailViewHolder(override val containerView: View):RecyclerView.ViewHolder(containerView),LayoutContainer{
        fun bind(totalPrice:Int,shippingCost:Int,payablePrice:Int){
            containerView.totalPriceTv.text = formatPrice(totalPrice)
            containerView.shippingCostTv.text = formatPrice(shippingCost)
            containerView.payablePriceTv.text = formatPrice(payablePrice)
        }
    }

    interface CartItemViewCallBacks{
        fun onRemoveCartItemButtonClick(cartItem: CartItem)
        fun onIncreaseCartItemButtonClick(cartItem: CartItem)
        fun onDecreaseCartItemButtonClick(cartItem: CartItem)
        fun onProductImageClick(cartItem: CartItem)
    }

    fun removeFromCartItem(cartItem: CartItem){
       val indexOf = cartItems.indexOf(cartItem)
        if (indexOf>-1){
            cartItems.removeAt(indexOf)
            notifyItemRemoved(indexOf)
        }
    }

    fun increaseCount(cartItem: CartItem){
        val indexOf = cartItems.indexOf(cartItem)
        if (indexOf>-1){
            cartItems[indexOf].changeCountProgressBarIsVisible = false
            notifyItemChanged(indexOf)
        }
    }
    fun decreaseCount(cartItem: CartItem){
        val indexOf = cartItems.indexOf(cartItem)
        if (indexOf>-1){
            cartItems[indexOf].changeCountProgressBarIsVisible = false
            notifyItemChanged(indexOf)
        }
    }

}