package com.ar.nikestore.feature.product

import android.content.Intent
import android.graphics.Paint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ar.nikestore.R
import com.ar.nikestore.common.EXTRA_KEY_ID
import com.ar.nikestore.common.NikeActivity
import com.ar.nikestore.common.NikeCompletableObserver
import com.ar.nikestore.common.formatPrice
import com.ar.nikestore.data.Comment
import com.ar.nikestore.feature.product.comment.CommentListActivity
import com.ar.nikestore.services.ImageLoadingService
import com.ar.nikestore.view.scroll.ObservableScrollView
import com.ar.nikestore.view.scroll.ObservableScrollViewCallbacks
import com.ar.nikestore.view.scroll.ScrollState
import com.google.android.material.snackbar.Snackbar
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_product_detail.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import timber.log.Timber

class ProductDetailActivity : NikeActivity() {
    val productDetailViewModel:ProductDetailViewModel by viewModel{parametersOf(intent.extras)}
    val imageLoadingService :ImageLoadingService by inject()
    val commentAdapter = CommentAdapter()
    val compositeDisposable:CompositeDisposable = CompositeDisposable()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)
        productDetailViewModel.productMutableLiveData.observe(this, Observer {
            imageLoadingService.load(productIv,it.image)
            titleTv.text = it.title
            previousPriceTv.text = formatPrice(it.previous_price)
            previousPriceTv.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
            currentPriceTv.text = formatPrice(it.price)
            toolBarTitleTv.text = it.title
        })

        productDetailViewModel.commentMutableLiveData.observe(this,Observer{
            commentAdapter.comments = it as ArrayList<Comment>
            if (it.size>3){
                viewAllCommentsBtn.visibility = View.VISIBLE
                viewAllCommentsBtn.setOnClickListener {
                    startActivity(Intent(this,CommentListActivity::class.java).apply {
                        putExtra(EXTRA_KEY_ID,
                            productDetailViewModel.productMutableLiveData.value!!.id)
                    })
                }
            }
        })

        productDetailViewModel.progressBarLiveData.observe(this, Observer {
            setProgressIndicator(it)
        })

        initViews()

    }

    fun initViews(){
        commentsRv.layoutManager = LinearLayoutManager(this,RecyclerView.VERTICAL,false)
        commentsRv.adapter = commentAdapter

        productIv.post {
            val heightProductIv = productIv.height
            val toolbar = toolbarView
            val productImageView = productIv
            observableScrollView.addScrollViewCallbacks(object : ObservableScrollViewCallbacks{
                override fun onUpOrCancelMotionEvent(scrollState: ScrollState?) {
                }

                override fun onScrollChanged(
                    scrollY: Int,
                    firstScroll: Boolean,
                    dragging: Boolean
                ) {
                    toolbar.alpha = scrollY.toFloat()/heightProductIv.toFloat()
                    productImageView.translationY = scrollY.toFloat()/2
                }

                override fun onDownMotionEvent() {

                }

            })
        }

        addToCartBtn.setOnClickListener {
            productDetailViewModel.onAddToCartBtn()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(object : NikeCompletableObserver(compositeDisposable){
                        override fun onComplete() {
                            showSnackBar(getString(R.string.addedToCart))
                        }
                    })
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }
}