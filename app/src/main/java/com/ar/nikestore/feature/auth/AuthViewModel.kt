package com.ar.nikestore.feature.auth

import androidx.lifecycle.ViewModel
import com.ar.nikestore.common.NikeViewModel
import com.ar.nikestore.data.repo.UserRepository
import io.reactivex.Completable

class AuthViewModel(private val userRepository: UserRepository): NikeViewModel() {
    fun login(email:String,password:String):Completable{
        progressBarLiveData.value = true
        return userRepository.login(email ,password)
                .doFinally {
                    progressBarLiveData.postValue(false)
                }
    }

    fun signUp(email:String,password:String):Completable{
        progressBarLiveData.value = true
        return userRepository.signUp(email ,password)
                .doFinally {
                    progressBarLiveData.postValue(false)
                }
    }

}