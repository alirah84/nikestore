package com.ar.nikestore.feature.product

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ar.nikestore.R
import com.ar.nikestore.data.Comment

class CommentAdapter(val showAll:Boolean = false): RecyclerView.Adapter<CommentAdapter.CommentViewHolder>() {
    var comments = ArrayList<Comment>()
    set(value) {
        field = value
        notifyDataSetChanged()
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
       return CommentViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_comment,parent,false))
    }

    override fun getItemCount(): Int {
        return if (comments.size>3 && !showAll) 3 else comments.size
    }

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {
        holder.bindComments(comments[position])
    }

    inner class CommentViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView){
        val commentTitleTv = itemView.findViewById<TextView>(R.id.commentTitleTv)
        val commentDateTv = itemView.findViewById<TextView>(R.id.commentDateTv)
        val commentAuthorTv = itemView.findViewById<TextView>(R.id.commentAuthor)
        val commentContentTv = itemView.findViewById<TextView>(R.id.commentContent)

        fun bindComments(comment: Comment){
            commentTitleTv.text = comment.title
            commentDateTv.text = comment.date
            commentAuthorTv.text = comment.author.email
            commentContentTv.text = comment.content
        }

    }

}