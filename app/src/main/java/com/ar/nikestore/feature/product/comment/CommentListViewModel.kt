package com.ar.nikestore.feature.product.comment

import androidx.lifecycle.MutableLiveData
import com.ar.nikestore.common.NikeSingleObserver
import com.ar.nikestore.common.NikeViewModel
import com.ar.nikestore.common.asyncNetworkRequest
import com.ar.nikestore.data.Comment
import com.ar.nikestore.data.repo.CommentRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class CommentListViewModel(productId:Int,commentRepository: CommentRepository) : NikeViewModel() {
    val comments = MutableLiveData<List<Comment>>()
    init {
        progressBarLiveData.value = true
        commentRepository.getAll(productId)
            .asyncNetworkRequest()
            .doFinally{
                progressBarLiveData.value = false
            }
            .subscribe(object : NikeSingleObserver<List<Comment>>(compositeDisposable){
                override fun onSuccess(t: List<Comment>) {
                    comments.value = t
                }

            })
    }
}