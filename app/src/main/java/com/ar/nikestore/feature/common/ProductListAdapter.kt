package com.ar.nikestore.feature.common

import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ar.nikestore.R
import com.ar.nikestore.common.formatPrice
import com.ar.nikestore.common.implementSpringAnimationTrait
import com.ar.nikestore.data.Product
import com.ar.nikestore.services.ImageLoadingService
import com.ar.nikestore.view.NikeImageView
import java.lang.IllegalStateException

const val VIEW_TYPE_ROUND = 0
const val VIEW_TYPE_SMALL = 1
const val VIEW_TYPE_LARGE = 2

class ProductListAdapter(var viewType: Int = VIEW_TYPE_ROUND, val imageLoadingService: ImageLoadingService) : RecyclerView.Adapter<ProductListAdapter.ProductViewHolder>() {
    var onProductClickListener: OnProductClickListener? = null
    var products = ArrayList<Product>()
    set(value) {
        field = value
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val layoutResId = when (viewType){
            VIEW_TYPE_ROUND ->R.layout.item_product
            VIEW_TYPE_LARGE -> R.layout.item_product_large
            VIEW_TYPE_SMALL -> R.layout.item_product_small
            else -> throw IllegalStateException("viewType is not valid")
        }
        return ProductViewHolder(LayoutInflater.from(parent.context).inflate(layoutResId,parent,false))
    }

    override fun getItemViewType(position: Int): Int {
        return viewType
    }

    override fun getItemCount(): Int = products.size

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) = holder.bindProduct(products[position])

    inner class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val productIv:NikeImageView = itemView.findViewById(R.id.productIv)
        val productTitleTv:TextView = itemView.findViewById(R.id.productTitleTv)
        val previousPrice:TextView = itemView.findViewById(R.id.previousPriceTv)

        val currentPriceTv :TextView = itemView.findViewById(R.id.currentPriceTv)
        fun bindProduct(product:Product){
            imageLoadingService.load(productIv,product.image)
            productTitleTv.text = product.title
            previousPrice.text = formatPrice(product.previous_price)
            previousPrice.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
            currentPriceTv.text = formatPrice(product.price)

            itemView.implementSpringAnimationTrait()
            itemView.setOnClickListener {
                onProductClickListener?.onProductClick(product)
            }
        }

    }

    interface OnProductClickListener{
        fun onProductClick(product:Product)
    }
}