package com.ar.nikestore.feature.product.comment

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.ar.nikestore.R
import com.ar.nikestore.common.EXTRA_KEY_ID
import com.ar.nikestore.common.NikeActivity
import com.ar.nikestore.data.Comment
import com.ar.nikestore.feature.product.CommentAdapter
import kotlinx.android.synthetic.main.activity_comment_list.*
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class CommentListActivity : NikeActivity() {
    val commentListViewModel:CommentListViewModel by viewModel{ parametersOf(intent.extras!!.getInt(EXTRA_KEY_ID))}
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comment_list)
        commentListViewModel.comments.observe(this, Observer {
            val commentAdapter = CommentAdapter(true)
            commentsRv.adapter = commentAdapter
            commentAdapter.comments = it as ArrayList<Comment>
            commentsRv.layoutManager = LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)
        })

        commentListToolbar.onBackButtonClickListener = View.OnClickListener {
            finish()
        }

        commentListViewModel.progressBarLiveData.observe(this, Observer {
            setProgressIndicator(it)
        })
    }
}