package com.ar.nikestore.feature.list

import androidx.lifecycle.MutableLiveData
import com.ar.nikestore.R
import com.ar.nikestore.common.NikeSingleObserver
import com.ar.nikestore.common.NikeViewModel
import com.ar.nikestore.common.asyncNetworkRequest
import com.ar.nikestore.data.Product
import com.ar.nikestore.data.repo.ProductRepository

class ProductListViewModel(var sort:Int,val productRepository: ProductRepository) : NikeViewModel() {
    val productListMutableLiveData = MutableLiveData<List<Product>>()
    val selectedSortTitleLiveData = MutableLiveData<Int>()
    val sortTitle = arrayOf(R.string.sortLatest,R.string.sortPopular,R.string.sortPriceHighToLow,R.string.sortPriceLowToHigh)
    init {
        getProducts()
        selectedSortTitleLiveData.value = sortTitle[sort]
    }

    fun getProducts(){
        progressBarLiveData.value = true
        productRepository.getProducts(sort)
            .asyncNetworkRequest()
            .doFinally { progressBarLiveData.value = false}
            .subscribe(object : NikeSingleObserver<List<Product>>(compositeDisposable){
                override fun onSuccess(t: List<Product>) {
                    productListMutableLiveData.value= t
                }

            })
    }

    fun onSelectedSortChangeByUser(sort:Int){
        this.sort = sort
        getProducts()
        selectedSortTitleLiveData.value=sortTitle[sort]
    }
}