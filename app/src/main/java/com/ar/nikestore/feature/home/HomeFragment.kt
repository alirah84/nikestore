package com.ar.nikestore.feature.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ar.nikestore.R
import com.ar.nikestore.common.EXTRA_KEY_DATA
import com.ar.nikestore.common.NikeFragment
import com.ar.nikestore.common.convertDpToPixel
import com.ar.nikestore.data.Product
import com.ar.nikestore.data.SORT_LATEST
import com.ar.nikestore.data.SORT_POPULAR
import com.ar.nikestore.feature.common.ProductListAdapter
import com.ar.nikestore.feature.common.VIEW_TYPE_ROUND
import com.ar.nikestore.feature.list.ProductListActivity
import com.ar.nikestore.feature.product.ProductDetailActivity
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import timber.log.Timber


class HomeFragment: NikeFragment(),
    ProductListAdapter.OnProductClickListener {
    val homeViewModel: HomeViewModel by viewModel()
    val latestProductListAdapter: ProductListAdapter by inject { parametersOf(VIEW_TYPE_ROUND)}
    val popularProductsListAdapter: ProductListAdapter by inject { parametersOf(VIEW_TYPE_ROUND)}
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        latestProductsRv.layoutManager = LinearLayoutManager(requireContext(),RecyclerView.HORIZONTAL,false)
        latestProductsRv.adapter = latestProductListAdapter
        latestProductListAdapter.onProductClickListener = this

        popularProductsRv.layoutManager = LinearLayoutManager(requireContext(),RecyclerView.HORIZONTAL,false)
        popularProductsRv.adapter = popularProductsListAdapter
        popularProductsListAdapter.onProductClickListener = this


        homeViewModel.latestProductsMutableLiveData.observe(viewLifecycleOwner, Observer {
            latestProductListAdapter.products = it as ArrayList<Product>
        })

        homeViewModel.popularProductsMutableLiveData.observe(viewLifecycleOwner, Observer<List<Product>> {
            popularProductsListAdapter.products = it as ArrayList<Product>
        })

        homeViewModel.progressBarLiveData.observe(viewLifecycleOwner, Observer {
            setProgressIndicator(it)
        })

        homeViewModel.bannerMutableLiveData.observe(viewLifecycleOwner,Observer{
            Timber.i(it.toString())
            val bannerSliderAdapter =
                BannerSliderAdapter(this, it)
            bannerSliderViewPager.adapter = bannerSliderAdapter
           val viewPagerHeight = ((bannerSliderViewPager.measuredWidth- convertDpToPixel(32f,requireContext()))*173)/328
          val layoutParams = bannerSliderViewPager.layoutParams
            layoutParams.height = viewPagerHeight.toInt()
            bannerSliderViewPager.layoutParams = layoutParams

            sliderIndicator.setViewPager2(bannerSliderViewPager)

        })

        viewLatestProductsBtn.setOnClickListener {
            startActivity(Intent(requireContext(),ProductListActivity::class.java).apply {
                putExtra(EXTRA_KEY_DATA, SORT_LATEST)
            })
        }

        viewPopularProductsBtn.setOnClickListener {
            startActivity(Intent(requireContext(),ProductListActivity::class.java).apply {
                putExtra(EXTRA_KEY_DATA, SORT_POPULAR)
            })
        }
    }

    override fun onProductClick(product: Product) {
        startActivity(Intent(requireContext(),ProductDetailActivity::class.java).apply {
            putExtra(EXTRA_KEY_DATA,product)
        })
    }
}