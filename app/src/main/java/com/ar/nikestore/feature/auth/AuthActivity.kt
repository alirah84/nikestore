package com.ar.nikestore.feature.auth

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import com.ar.nikestore.R
import com.ar.nikestore.common.NikeActivity
import org.koin.android.viewmodel.ext.android.viewModel

class AuthActivity : NikeActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragment_container,LoginFragment())
        }.commit()

    }
}