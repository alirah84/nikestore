package com.ar.nikestore.feature.list

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.ar.nikestore.R
import com.ar.nikestore.common.EXTRA_KEY_DATA
import com.ar.nikestore.common.NikeActivity
import com.ar.nikestore.data.Product
import com.ar.nikestore.feature.common.ProductListAdapter
import com.ar.nikestore.feature.common.VIEW_TYPE_LARGE
import com.ar.nikestore.feature.common.VIEW_TYPE_SMALL
import com.ar.nikestore.feature.product.ProductDetailActivity
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.activity_product_list.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class ProductListActivity : NikeActivity(),ProductListAdapter.OnProductClickListener {
    val productListViewModel:ProductListViewModel by viewModel{parametersOf(intent.extras!!.getInt(EXTRA_KEY_DATA))}
    val productListAdapter: ProductListAdapter by inject{ parametersOf(
        VIEW_TYPE_SMALL
    )}
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_list)
        val gridLayoutManager = GridLayoutManager(this,2)
        productsRv.layoutManager = gridLayoutManager
        productListAdapter.onProductClickListener = this
        productsRv.adapter = productListAdapter

        productListViewModel.productListMutableLiveData.observe(this, Observer {
            productListAdapter.products = it as ArrayList<Product>
        })

        viewTypeChangerButton.setOnClickListener {
            if (productListAdapter.viewType == VIEW_TYPE_SMALL){
                productListAdapter.viewType = VIEW_TYPE_LARGE
                gridLayoutManager.spanCount = 1
                viewTypeChangerButton.setImageResource(R.drawable.ic_view_type_large)
                productListAdapter.notifyDataSetChanged()
            }

            else
            {
                productListAdapter.viewType = VIEW_TYPE_SMALL
                gridLayoutManager.spanCount = 2
                viewTypeChangerButton.setImageResource(R.drawable.ic_grid)
                productListAdapter.notifyDataSetChanged()
            }
        }

        sortBtn.setOnClickListener {
            MaterialAlertDialogBuilder(this)
                .setSingleChoiceItems(R.array.sortTitlesArray,productListViewModel.sort
                ) { dialog, selectedSortIndex ->
                    productListViewModel.onSelectedSortChangeByUser(selectedSortIndex)
                    dialog.dismiss()
                }
                .setTitle(getString(R.string.sort))
                .show()
        }

        productListViewModel.selectedSortTitleLiveData.observe(this, Observer {
            selectedSortTitleTv.text = getString(it)
        })

        productListViewModel.progressBarLiveData.observe(this, Observer {
            setProgressIndicator(it)
        })

        toolbarView.onBackButtonClickListener = View.OnClickListener {
            finish()
        }

    }

    override fun onProductClick(product: Product) {
        startActivity(Intent(this,ProductDetailActivity::class.java).apply {
            putExtra(EXTRA_KEY_DATA,product)
        })
    }
}