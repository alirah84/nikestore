package com.ar.nikestore.feature.home

import androidx.lifecycle.MutableLiveData
import com.ar.nikestore.common.NikeSingleObserver
import com.ar.nikestore.common.NikeViewModel
import com.ar.nikestore.data.Banner
import com.ar.nikestore.data.Product
import com.ar.nikestore.data.SORT_LATEST
import com.ar.nikestore.data.SORT_POPULAR
import com.ar.nikestore.data.repo.BannerRepository
import com.ar.nikestore.data.repo.ProductRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class HomeViewModel(productRepository: ProductRepository, bannerRepository: BannerRepository): NikeViewModel() {
    val latestProductsMutableLiveData = MutableLiveData<List<Product>>()
    val popularProductsMutableLiveData = MutableLiveData<List<Product>>()
    val bannerMutableLiveData = MutableLiveData<List<Banner>>()
    init {
        progressBarLiveData.value = true
        productRepository.getProducts(SORT_LATEST)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally { progressBarLiveData.value=false }
            .subscribe(object : NikeSingleObserver<List<Product>>(compositeDisposable){
                override fun onSuccess(t: List<Product>) {
                    latestProductsMutableLiveData.value = t
                }

            })

        productRepository.getProducts(SORT_POPULAR)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : NikeSingleObserver<List<Product>>(compositeDisposable){
                override fun onSuccess(t: List<Product>) {
                    popularProductsMutableLiveData.value = t
                }

            })


        bannerRepository.getBanners()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : NikeSingleObserver<List<Banner>>(compositeDisposable){
                override fun onSuccess(t: List<Banner>) {
                    bannerMutableLiveData.value=t
                }

            })

    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}