package com.ar.nikestore.feature.cart

import androidx.lifecycle.MutableLiveData
import com.ar.nikestore.common.NikeSingleObserver
import com.ar.nikestore.common.NikeViewModel
import com.ar.nikestore.common.asyncNetworkRequest
import com.ar.nikestore.data.CartItem
import com.ar.nikestore.data.CartResponse
import com.ar.nikestore.data.PurchaseDetail
import com.ar.nikestore.data.TokenContainer
import com.ar.nikestore.data.repo.CartRepository
import io.reactivex.Completable

class CartViewModel(val cartRepository: CartRepository): NikeViewModel() {
    val cartItemsLiveData = MutableLiveData<List<CartItem>>()
    val purchaseDetailLiveData = MutableLiveData<PurchaseDetail>()
   private fun getCartItems(){
        if (!TokenContainer.token.isNullOrEmpty()){
            progressBarLiveData.value = true
            cartRepository.get()
                    .asyncNetworkRequest()
                    .doFinally {
                        progressBarLiveData.value = false
                    }
                    .subscribe(object :NikeSingleObserver<CartResponse>(compositeDisposable){
                        override fun onSuccess(t: CartResponse) {
                            if (t.cart_items.isNotEmpty()){
                                cartItemsLiveData.value = t.cart_items
                                purchaseDetailLiveData.value = PurchaseDetail(t.total_price,t.payable_price,t.shipping_cost)
                            }
                        }

                    })

        }
    }

    fun removeItemFromCart(cartItem: CartItem):Completable = cartRepository.remove(cartItem.cart_item_id)
            .doAfterSuccess {
                calculateAndPublishPurchaseDetail()
            }
            .ignoreElement()

    fun increaseCartItemCount(cartItem: CartItem):Completable = cartRepository.changeCount(cartItem.cart_item_id,++cartItem.count)
            .doAfterSuccess {
                calculateAndPublishPurchaseDetail()
            }
            .ignoreElement()

    fun decreaseCartItemCount(cartItem: CartItem):Completable = cartRepository.changeCount(cartItem.cart_item_id,--cartItem.count)
            .doAfterSuccess {
                calculateAndPublishPurchaseDetail()
            }
            .ignoreElement()

    fun refresh(){
        getCartItems()
    }

    private fun calculateAndPublishPurchaseDetail(){
        cartItemsLiveData.value?.let{cartItems->
            purchaseDetailLiveData.value?.let {purchaseDetail ->
                var totalPrice = 0
                var payablePrice = 0
                cartItems.forEach {
                    totalPrice+=it.product.price*it.count
                    payablePrice+=(it.product.price-it.product.discount)*it.count
                }
                purchaseDetail.totalPrice =totalPrice
                purchaseDetail.payablePrice = payablePrice
                purchaseDetailLiveData.postValue(purchaseDetail)
            }
        }
    }
}