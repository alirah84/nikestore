package com.ar.nikestore.feature.cart

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ar.nikestore.R
import com.ar.nikestore.common.EXTRA_KEY_DATA
import com.ar.nikestore.common.NikeCompletableObserver
import com.ar.nikestore.common.NikeFragment
import com.ar.nikestore.data.CartItem
import com.ar.nikestore.feature.product.ProductDetailActivity
import com.ar.nikestore.services.ImageLoadingService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_cart.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

class CartFragment: NikeFragment(),CartItemAdapter.CartItemViewCallBacks {
    val cartViewModel:CartViewModel by viewModel()
    var cartItemAdapter:CartItemAdapter? = null
    val imageLoadingService:ImageLoadingService by inject()
    val compositeDisposable:CompositeDisposable = CompositeDisposable()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_cart,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cartViewModel.progressBarLiveData.observe(viewLifecycleOwner, Observer {
            setProgressIndicator(it)
        })

        cartViewModel.cartItemsLiveData.observe(viewLifecycleOwner, Observer {
            cartItemAdapter = CartItemAdapter(it as MutableList<CartItem>,this,imageLoadingService)
            cartRv.layoutManager = LinearLayoutManager(requireContext(),RecyclerView.VERTICAL,false)
            cartRv.adapter = cartItemAdapter
        })

        cartViewModel.purchaseDetailLiveData.observe(viewLifecycleOwner, Observer {
            cartItemAdapter?.let {adapter ->
                adapter.purchaseDetail = it
                adapter.notifyItemChanged(adapter.cartItems.size)
            }
        })
    }

    override fun onStart() {
        super.onStart()
        cartViewModel.refresh()
    }

    override fun onRemoveCartItemButtonClick(cartItem: CartItem) {
        cartViewModel.removeItemFromCart(cartItem)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object :NikeCompletableObserver(compositeDisposable){
                    override fun onComplete() {
                        cartItemAdapter?.removeFromCartItem(cartItem)
                    }

                })
    }

    override fun onIncreaseCartItemButtonClick(cartItem: CartItem) {
        cartViewModel.increaseCartItemCount(cartItem)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object :NikeCompletableObserver(compositeDisposable){
                    override fun onComplete() {
                        cartItemAdapter?.increaseCount(cartItem)
                    }

                })
    }

    override fun onDecreaseCartItemButtonClick(cartItem: CartItem) {
        cartViewModel.decreaseCartItemCount(cartItem)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object :NikeCompletableObserver(compositeDisposable){
                    override fun onComplete() {
                        cartItemAdapter?.decreaseCount(cartItem)
                    }

                })
    }

    override fun onProductImageClick(cartItem: CartItem) {
        startActivity(Intent(requireContext(),ProductDetailActivity::class.java).apply {
            putExtra(EXTRA_KEY_DATA,cartItem.product)
        })
    }
}