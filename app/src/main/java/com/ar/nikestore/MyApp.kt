package com.ar.nikestore

import android.util.Log
import javax.security.auth.login.LoginException

class EmailService : MessageService{
    override fun sendMessage(message: String, receiver: String) {
        println("<$message> send to <$receiver>")
    }

}
class SmsService : MessageService{
    override fun sendMessage(message: String, receiver: String) {
        println("<$message> send to <$receiver>")
    }

}

interface MessageService{
    fun sendMessage(message:String, receiver:String)
}

class Activity1(val messageService:MessageService){
    fun processMessage(message:String, receiver:String){
        messageService.sendMessage(message,receiver)
    }
}
class Activity2(val messageService:MessageService){
    fun processMessage(message:String, receiver:String){
        messageService.sendMessage(message,receiver)
    }
}

object Injector{
    fun inject():MessageService{
        return SmsService()
    }
}

fun main(){
val activity1 = Activity1(Injector.inject())
    activity1.processMessage("Test1","Ali Rahnama")
    val activity2 = Activity2(Injector.inject())
    activity2.processMessage("Test2","Ali Ahmadi")
}

interface HttpClient{
   fun sendRequest(url:String,method:String)
}

class Retrofit:HttpClient{
    override fun sendRequest(url: String, method: String) {
        Log.i("Retrofit", "sendHttpRequestFromRetrofit")
    }

}

class Volley:HttpClient{
    override fun sendRequest(url: String, method: String) {
        Log.i("Volley", "sendHttpRequestFromVolley")
    }

}

object HttpInjector{
    fun inject():HttpClient{
        return Volley()
    }
}